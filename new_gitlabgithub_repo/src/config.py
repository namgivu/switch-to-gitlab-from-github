from dotenv import load_dotenv
import os


load_dotenv()
GITLAB_USER          = os.environ.get('GITLAB_USER')
GITLAB_PASS          = os.environ.get('GITLAB_PASS')
NEW_PROJECT_NAME     = os.environ.get('NEW_PROJECT_NAME')
NEW_PROJECT_ISPUBLIC = os.environ.get('NEW_PROJECT_ISPUBLIC')
GITHUB_USER          = os.environ.get('GITHUB_USER')
GITHUB_PASS          = os.environ.get('GITHUB_PASS')
