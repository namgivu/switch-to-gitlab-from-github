from time import sleep

from src.service.webdriver import load_webdriver_f_downloaded_chromewd
from src.service.wd_helper import wait_by_id, wait_by_xpath

from src.config import GITHUB_USER, GITHUB_PASS, NEW_PROJECT_NAME, NEW_PROJECT_ISPUBLIC


wd = load_webdriver_f_downloaded_chromewd()  # wd aka webdriver

#region login github
wd.get('https://github.com/login')

wait_by_id    (wd, 'login_field')               .send_keys(GITHUB_USER)
wait_by_id    (wd, 'password')                  .send_keys(GITHUB_PASS)
wait_by_xpath (wd, "//*[@value = 'Sign in']")   .click()

input('Complete log in as guided on the web. Enter to continue.')
#endregion login github

print('Thank you for enter. Now proceed project creation...')

#region new_GITHUB_project
wd.get('https://github.com/new')

wait_by_id(wd, 'repository_name').send_keys(NEW_PROJECT_NAME)

#region prj_visibility
if NEW_PROJECT_ISPUBLIC=='1':
    prj_visibility = wait_by_xpath(wd, "//*[@id='repository_visibility_public']")
else:
    prj_visibility = wait_by_xpath(wd, "//*[@id='repository_visibility_private']")
prj_visibility.click()
#endregion prj_visibility

sleep(1)
wait_by_xpath(wd, "//button[contains(text(), 'Create repository')]").click()
#endregion new_GITHUB_project

input('Now proceed project creation... DONE. Please press enter to end.')
wd.quit()
