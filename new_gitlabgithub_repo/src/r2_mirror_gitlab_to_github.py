from src.service.webdriver import load_webdriver_f_downloaded_chromewd
from src.service.wd_helper import wait_by_id, wait_by_xpath

from src.config import GITLAB_USER, GITLAB_PASS, NEW_PROJECT_NAME, GITHUB_USER, GITHUB_PASS


wd = load_webdriver_f_downloaded_chromewd()  # wd aka webdriver

#region login gitlab
wd.get('https://gitlab.com/users/sign_in')

wait_by_id    (wd, 'user_login')                .send_keys(GITLAB_USER)
wait_by_id    (wd, 'user_password')             .send_keys(GITLAB_PASS)
wait_by_xpath (wd, "//*[@value = 'Sign in']")   .click()

input('Complete log in as guided on the web. Enter to continue.')
#endregion login gitlab

print('Thank you for enter. Now proceed project mirror...')

#region mirror gitlab repo to github
wd.get(f'https://gitlab.com/{GITLAB_USER}/{NEW_PROJECT_NAME}/-/settings/repository')

wait_by_xpath(wd, "//section[@id='js-push-remote-settings']//button[contains(text(),'Expand')]").click()
#                  .                                       .

pushee_github_repo_url=f'https://{GITHUB_USER}@github.com/{GITLAB_USER}/{NEW_PROJECT_NAME}.git'

# set pull
wait_by_xpath(wd, "//*[@id='url']").send_keys(pushee_github_repo_url)
wait_by_xpath(wd, "//option[@value='pull']")                            .click()
wait_by_xpath(wd, "//*[@id='project_import_data_attributes_password']") .send_keys(GITHUB_PASS)
wait_by_xpath(wd, "//*[@name='update_remote_mirror']")                  .click()

# set push
wait_by_xpath(wd, "//*[@id='url']").send_keys(pushee_github_repo_url)
wait_by_xpath(wd, "//option[@value='push']")                                 .click()
wait_by_xpath(wd, "//*[@id='project_remote_mirrors_attributes_0_password']") .send_keys(GITHUB_PASS)
wait_by_xpath(wd, "//*[@name='update_remote_mirror']")                       .click()

#endregion mirror gitlab repo to github

input('Now proceed project mirror... DONE. Please press enter to end.')
wd.quit()
