from time import sleep

from src.config import GITLAB_USER, GITLAB_PASS, NEW_PROJECT_NAME, NEW_PROJECT_ISPUBLIC
from src.service.webdriver import load_webdriver_f_autoinstall_chrome_wd
from src.service.wd_helper import wait_by_id, wait_by_xpath


wd = load_webdriver_f_autoinstall_chrome_wd()  # wd aka webdriver

#region login gitlab
wd.get('https://gitlab.com/users/sign_in')  #TODO gitlab today seem having browser-check to anti automation :D
sleep(6)

wait_by_id    (wd, 'user_login')                .send_keys(GITLAB_USER)
wait_by_id    (wd, 'user_password')             .send_keys(GITLAB_PASS)
wait_by_xpath (wd, "//*[@value = 'Sign in']")   .click()

input('Complete log in as guided on the web. Enter to continue.')
#endregion login gitlab

print('Thank you for enter. Now proceed project creation...')

#region new_gitlab_project
wd.get('https://gitlab.com/projects/new#blank_project')

wait_by_id(wd, 'project_name').send_keys(NEW_PROJECT_NAME)

#region prj_visibility
if NEW_PROJECT_ISPUBLIC=='1':
    prj_visibility = wait_by_xpath(wd, "//div[contains(text(), 'Public') and @class='option-title']")
else:
    prj_visibility = wait_by_xpath(wd, "//div[contains(text(), 'Private') and @class='option-title']")
prj_visibility.click()
#endregion prj_visibility

wait_by_xpath(wd, "//*[text() = 'Initialize repository with a README']").click()
wait_by_xpath(wd, "//*[@value = 'Create project']")                     .click()
#endregion new_gitlab_project

input('Now proceed project creation... DONE. Please press enter to end.')
wd.quit()
