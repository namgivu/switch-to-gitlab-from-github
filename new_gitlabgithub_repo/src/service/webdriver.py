from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


WD_IMPLICITLY_WAIT=6


def load_webdriver_f_autoinstall_chrome_wd():  # f aka from
    wd = webdriver.Chrome(ChromeDriverManager().install())
    wd.implicitly_wait(WD_IMPLICITLY_WAIT)
    wd.maximize_window()
    return wd
