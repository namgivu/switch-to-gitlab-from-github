"""
wd_helper aka webdriver helper

webdriver wait for visible python ref. https://stackoverflow.com/a/40708217
"""

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


WDW_TIMEOUT=6

def wait_by_id(wd:'webdriver', id):
    wdw = WebDriverWait(wd, timeout=WDW_TIMEOUT)  # wdw aka wd_wait
    e = wdw.until(EC.visibility_of_element_located((By.ID, id)))  # e aka element
    return e


def wait_by_xpath(wd:'webdriver', xpath):
    wdw = WebDriverWait(wd, timeout=WDW_TIMEOUT)  # wdw aka wd_wait
    e = wdw.until(EC.visibility_of_element_located((By.XPATH, xpath)))  # e aka element
    return e
