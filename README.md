# switch-to-gitlab-from-github

When microsoft acquire/buy github, it's a weird/bad/nightmare feeling for me.
So I switch to gitlab but need my code cloned/synced/broadcast to github cause I've been building my engineer credit there for years.

This repo is for me to take note during this move


# steps

00a create the repo in gitlab   eg https://gitlab.com/namgivu/browserstack-start

00b create the repo in github   eg https://github.com/namgivu/browserstack-start - caution: MUST be BLANK repo no readme file, no description

01  enable sync from gitlab to github [view](https://gitlab.com/namgivu/switch-to-gitlab-from-github/-/blob/master/README.md#how-to-sync-a-repo-from-gitlab-to-github)

02  enable sync from github to gitlab [view](https://gitlab.com/namgivu/switch-to-gitlab-from-github/-/blob/master/README.md#how-to-sync-back-ie-a-repo-from-github-to-gitlab)


# how to sync a repo from gitlab to github

Use gitlab repo's **mirror-repository** with `push` action as guided [here](https://stackoverflow.com/a/38303300/248616)

NOTE when entering github repo ULR it should be 
```
https://$USER@github.com/$USER/$REPO_NAME.git
#       you              you   repo      .git <-MUST HAVE--
```

At password box, enter your github logic password - NOTE this is github NOT gitlab password

View steps in recorded [here](./gitlab-github-sync.part2-repo-miror-f-gitlab.mp4)


# how to sync back ie a repo from github to gitlab

Same thing, just use `pull` action similarly to `push`
